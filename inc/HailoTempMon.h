#pragma once

#include <hailo/hailort.hpp>

#include <algorithm>
#include <chrono>
#include <iostream>
#include <mutex>
#include <thread>
#include <vector>

#include "Utils.h"

DEFINE_EXCEPTION(HailoTempMonException)

static constexpr std::chrono::milliseconds DEFAULT_TEMPERATURE_MEASUREMENTS_INTERVAL(1000);
static constexpr float32_t DEFAULT_TEMPERATURE_WARNING_THRESHOLD = 70.0;

class HailoTempMon
{
	struct TempMeasurementData
	{
		float32_t averageValue;
		float32_t minValue;
		float32_t maxValue;
		uint32_t sampleCount;
	};

public:
	HailoTempMon(hailort::Device& physDev, const float32_t& warningTemp = DEFAULT_TEMPERATURE_WARNING_THRESHOLD, const std::chrono::milliseconds& interval = DEFAULT_TEMPERATURE_MEASUREMENTS_INTERVAL) :
		m_physDev(physDev),
		m_warningTemp(warningTemp),
		m_interval(interval)
	{
	}

	~HailoTempMon()
	{
		stopMonitoring();
	}

	void startMonitoring()
	{
		// Checking temperature sensor before starting thread
		auto tempInfo = m_physDev.get_chip_temperature();
		if (tempInfo.status() != HAILO_SUCCESS)
			throwError("Failed to get chip's temperature", tempInfo.status());

		m_isThreadRunning = true;

		m_thread = std::thread(
			[this]()
			{
				while (m_isThreadRunning.load())
				{
					auto tempInfo = m_physDev.get_chip_temperature();
					if (tempInfo.status() != HAILO_SUCCESS)
					{
						m_isThreadRunning = false;
						throwError("Failed to get chip's temperature", tempInfo.status());
					}

					TempMeasurementData newData = {};
					auto oldData                = m_data;

					float32_t tsAvg       = ((tempInfo->ts0_temperature + tempInfo->ts1_temperature) / 2);
					newData.maxValue     = std::max(oldData.maxValue, tsAvg);
					newData.minValue     = (oldData.minValue == 0) ? tsAvg : std::min(oldData.minValue, tsAvg);
					newData.averageValue = calcAvg(oldData.sampleCount, oldData.averageValue, tempInfo->sample_count, tsAvg);
					newData.sampleCount  = oldData.sampleCount + tempInfo->sample_count;

					{
						std::unique_lock<std::mutex> lock(m_mutex);
						m_data = newData;
					}

					if(tsAvg > m_warningTemp)
						std::cerr << string_format("WARNING: Temperature is above threshold (%.2f > %.2f)", tsAvg, m_warningTemp) << std::endl;

					std::this_thread::sleep_for(m_interval);
				}
			});
	}

	void stopMonitoring()
	{
		m_isThreadRunning = false;

		if (m_thread.joinable())
			m_thread.join();
	}

private:
	static inline void throwError(const std::string& msg, const hailo_status& status)
	{
		throw(HailoTempMonException(string_format("%s - Status=%d", msg.c_str(), status)));
	}

	static float32_t calcAvg(const uint32_t& oldSamplesCount, const float32_t& oldAvg, const uint32_t& newSamplesCount, const float32_t& newValue)
	{
		float32_t oldSamples        = static_cast<float32_t>(oldSamplesCount);
		float32_t newSamples        = static_cast<float32_t>(newSamplesCount);
		float32_t totalSamplesCount = oldSamples + newSamples;
		return (((oldAvg * oldSamples) + (newValue * newSamples)) / totalSamplesCount);
	}

private:
	hailort::Device& m_physDev;
	float32_t m_warningTemp;
	std::chrono::milliseconds m_interval;

	std::mutex m_mutex = std::mutex();
	TempMeasurementData m_data = {};
	std::thread m_thread = std::thread();
	std::atomic_bool m_isThreadRunning = false;
};
